all: run

run:
	ng serve

build:
	ng build --prod --base-href ${BASE_HREF}

deploy:
	cp scp ./dist/bamboo-site/scp && cd dist/bamboo-site/ && ./scp -u root -host 188.166.65.29 -p 22 -d ./ -rd /srv/data/bamboo && cd --

fix:
	sudo sysctl fs.inotify.max_user_watches=524288
	sudo sysctl -p --system
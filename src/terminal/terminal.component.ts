import { Component } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';

@Component({
    selector: 'bamboo-terminal',
    templateUrl: './terminal.component.html',
    styleUrls: ['./terminal.component.scss']
})
export class TerminalComponent {
    private _tabs: Tab[] = [
        {
            Label: "Новости",
            Path: "/terminal/welcome",
            Icon: "notification_important",
            Logo: "assets/img/bamboo.jpg",
            Title: "Добро пожаловать в IT-школу BAMBOO!",
            Subtitle: "Bamboo - это школа программирования для детей и взрослых.",
            TitleTyping: {
                TypingDelay: 200,
                TypingSpeed: 30
            },
            SubtitleTyping: {
                TypingDelay: 2000,
                TypingSpeed: 30
            }
        },
        {
            Label: "Python 3",
            Path: "/terminal/python3",
            Logo: "assets/img/python.png",
            Title: "Разработка приложений на Python 3.",
            Subtitle: "Python стабильно входит в ТОП-10 наиболее популярных языков программирования. Это именно тот язык с которого стоит начинать изучать программирование. Благодаря своей простоте и элегантности, Python позволяет новичкам не вникать во множество сложных программных понятий и конструкций, присущих другим языкам.",
            TitleTyping: {
                TypingDelay: 200,
                TypingSpeed: 30
            },
            SubtitleTyping: {
                TypingDelay: 2000,
                TypingSpeed: 15
            }
        },
        {
            Label: "Scratch 3",
            Path: "/terminal/scratch3",
            Logo: "assets/img/scratch.png",
            Title: "Разработка игр на Scratch 3.",
            Subtitle: "Scratch - это начальный уровень программирования, который позволяет детям создавать собственные анимированные и интерактивные игры, презентации и проекты. Скретч позволяет детям составлять свои программы-процедуры из блоков так же легко, как если бы они собирали конструкции из разноцветных кубиков.",
            TitleTyping: {
                TypingDelay: 200,
                TypingSpeed: 30
            },
            SubtitleTyping: {
                TypingDelay: 2000,
                TypingSpeed: 15
            }
        },
        {
            Label: "WEB",
            Path: "/terminal/web",
            Logo: "assets/img/html.png",
            Title: "Разработка web-приложений на HTML5/JS/CSS3.",
            Subtitle: "Курс посвящен базовым технологиям веб-программирования – HTML5, CSS3, JS и рассчитан на людей с минимальными знаниями в области веб-технологий. Цель курса – научить \"с нуля\" создавать современные веб-интерфейсы, работая с кодом вручную. Вы сможете самостоятельно создавать веб-страницы начального и среднего уровня сложности.",
            TitleTyping: {
                TypingDelay: 200,
                TypingSpeed: 30
            },
            SubtitleTyping: {
                TypingDelay: 2000,
                TypingSpeed: 15
            }
        },
    ];
    public selectedTabIndex: number = 0;

    constructor(private _router: Router) {
        this._router.events.subscribe((event: Event) => {
            if(event instanceof NavigationEnd) {
                for (let i: number = 0; i < this._tabs.length; i++) {
                    if (this._tabs[i].Path.includes(event.url)) {
                        this.selectedTabIndex = i;
                        break;
                    }
                }
            }
        });
    }

    changeTabContent(i: number): void {
        this._router.navigate([this._tabs[i].Path]);
    }

    get tabs() : Tab[] {
        return this._tabs;
    }
}

interface Tab {
    Label: string;
    Path: string;
    Icon?: string;
    Logo: string;
    Title: string;
    Subtitle?: string;
    TitleTyping: TypingConfig;
    SubtitleTyping: TypingConfig;
}

interface TypingConfig {
    TypingSpeed: number;
    TypingDelay: number;
}
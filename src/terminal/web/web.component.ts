import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    templateUrl: './web.component.html',
    styleUrls: ['./web.component.scss']
})
export class WebComponent {
    constructor(private _router: Router) {}

    signUp(): void {
        this._router.navigate(['/signup/web']);
    }
}
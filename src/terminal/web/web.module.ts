import { NgModule } from '@angular/core';
import { WebComponent } from './web.component';
import { WebRoutingModule } from './web-routing.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [
        WebComponent
    ],
    imports: [
        WebRoutingModule,
        MatButtonModule
    ]
})
export class WebModule {}
import { NgModule } from '@angular/core';
import { Scratch3Component } from './scratch3.component';
import { Scratch3RoutingModule } from './scratch3-routing.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [
        Scratch3Component
    ],
    imports: [
        Scratch3RoutingModule,
        MatButtonModule
    ]
})
export class Scratch3Module {}
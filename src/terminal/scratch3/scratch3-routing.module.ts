import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Scratch3Component } from './scratch3.component';

const routes: Routes = [
    {
        path: "",
        component: Scratch3Component,
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Scratch3RoutingModule {}

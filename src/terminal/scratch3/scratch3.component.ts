import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    templateUrl: './scratch3.component.html',
    styleUrls: ['./scratch3.component.scss']
})
export class Scratch3Component {
    constructor(private _router: Router) {}

    signUp(): void {
        this._router.navigate(['/signup/scratch3']);
    }
}
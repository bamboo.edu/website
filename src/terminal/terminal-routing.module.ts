import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TerminalComponent } from './terminal.component';

const routes: Routes = [
	{
		path: "",
		component: TerminalComponent,
		children: [
			{ path: "", redirectTo: "/terminal/welcome", pathMatch: "full" },
			{ path: "welcome", loadChildren: "./welcome/welcome.module#WelcomeModule" },
			{ path: "python3", loadChildren: "./python3/python3.module#Python3Module" },
			{ path: "scratch3", loadChildren: "./scratch3/scratch3.module#Scratch3Module" },
			{ path: "web", loadChildren: "./web/web.module#WebModule" }
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class TerminalRoutingModule {}
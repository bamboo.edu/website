import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerminalComponent } from './terminal.component';
import { TerminalRoutingModule } from './terminal-routing.module';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { WelcomeModule } from './welcome/welcome.module';
import { TypingAnimationModule } from 'angular-typing-animation';

@NgModule({
    declarations: [
        TerminalComponent,
    ],
    imports: [
        TerminalRoutingModule,
        TypingAnimationModule,
        CommonModule,
        MatIconModule,
        MatTabsModule,
        WelcomeModule
    ]
})
export class TerminalModule {}
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    templateUrl: './python3.component.html',
    styleUrls: ['./python3.component.scss']
})
export class Python3Component {
    constructor(private _router: Router) {}

    signUp(): void {
        this._router.navigate(['/signup/python3']);
    }
}
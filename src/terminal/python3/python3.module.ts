import { NgModule } from '@angular/core';
import { Python3Component } from './python3.component';
import { Python3RoutingModule } from './python3-routing.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [
        Python3Component
    ],
    imports: [
        Python3RoutingModule,
        MatButtonModule
    ]
})
export class Python3Module {}
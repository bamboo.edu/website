import { NgModule } from '@angular/core';
import { WelcomeComponent } from './welcome.component';
import { WelcomeRoutingModule } from './welcome-routing.module';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        WelcomeComponent
    ],
    imports: [
        WelcomeRoutingModule,
        MatIconModule
    ]
})
export class WelcomeModule {}
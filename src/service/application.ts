import { Injectable } from '@angular/core';
import { Application } from '../model/application';

@Injectable()
export class ApplicationService {
    private _active: Application = null;

    get active() : Application {
        return this._active;
    }

    set active(app: Application) {
        this._active = app;
    } 
}
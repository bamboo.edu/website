import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Email } from '../model/email';

@Injectable()
export class MailService {
    constructor(private _http: HttpClient) {}

    send(email: Email) : Observable<any> {
        return this._http.post(`${environment.mailAPI}/mail/send`, email);
    }
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeachersComponent } from './teachers.component';
import { TeachersRoutingModule } from './teachers-routing.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
    declarations: [
        TeachersComponent,
    ],
    imports: [
        TeachersRoutingModule,
        CommonModule,
        MatGridListModule,
        MatIconModule,
        MatDividerModule
    ]
})
export class TeachersModule {}
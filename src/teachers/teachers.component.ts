import { Component } from '@angular/core';

@Component({
    templateUrl: './teachers.component.html',
    styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent {
    private _teachers: TeacherInfo[] = [
        {
            Photo: "assets/img/varga.jpg",
            Name: "Анастасия Варга",
            About: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            Contacts: {
                Email: "stais66@inbox.ru"
            }
        },
        {
            Photo: "assets/img/gutorov.jpg",
            Name: "Артём Гуторов",
            About: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            Contacts: {
                Email: "gutorov.artem@yandex.ru"
            }
        }
    ];

    get teachers() : TeacherInfo[] {
        return this._teachers;
    }
}

interface TeacherInfo {
    Photo: string;
    Name: string;
    About: string;
    Contacts?: {
        Email?: string;
    }
}
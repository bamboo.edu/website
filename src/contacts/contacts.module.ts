import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsComponent } from './contacts.component';
import { ContactsRoutingModule } from './contacts-routing.module';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        ContactsComponent,
    ],
    imports: [
        ContactsRoutingModule,
        CommonModule,
        MatListModule,
        MatIconModule
    ]
})
export class ContactsModule {}
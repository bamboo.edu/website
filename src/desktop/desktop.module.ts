import { NgModule } from '@angular/core';
import { DesktopRoutingModule } from './desktop-routing.module';
import { DesktopComponent } from './desktop.component';
import { TerminalModule } from '../terminal/terminal.module';
import { DockComponent } from '../dock/dock.component';
import { CommonModule } from '@angular/common';
import { WindowComponent } from '../window/window.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ApplicationService } from '../service/application';
import { TopBarComponent } from '../top-bar/top-bar.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';

@NgModule({
    declarations: [
        DesktopComponent,
        DockComponent,
        WindowComponent,
        TopBarComponent
    ],
    imports: [
        DesktopRoutingModule,
        TerminalModule,
        CommonModule,
        MatToolbarModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatTooltipModule,
        MalihuScrollbarModule.forChild()
    ],
    providers: [ApplicationService]
})
export class DesktopModule {}
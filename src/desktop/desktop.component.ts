import { Component, OnInit } from '@angular/core';
import { Application } from '../model/application';
import { ApplicationService } from '../service/application';

@Component({
    selector: 'bamboo-desktop',
    templateUrl: './desktop.component.html',
    styleUrls: ['./desktop.component.scss']
})
export class DesktopComponent implements OnInit {
    private _apps: Application[] = [
        {
            Icon: "assets/img/terminal.png",
            Path: "/terminal",
            Name: "Bamboo Terminal",
            Title: "Курсы"
        },
        {
            Icon: "assets/img/map.png",
            Path: "/location",
            Name: "Как добраться",
            Title: "Офис"
        },
        {
            Icon: "assets/img/contact.png",
            Path: "/contacts",
            Name: "Контакты",
            Title: "Контакты"
        },
        {
            Icon: "assets/img/signup.png",
            Path: "/signup",
            Name: "Подать заявку",
            Title: "Записаться"
        },
        {
            Icon: "assets/img/teachers.png",
            Path: "/teachers",
            Name: "Преподаватели школы BAMBOO",
            Title: "Преподаватели"
        }
    ];

    constructor(private _appService: ApplicationService) {}

    apps() : Application[] {
        return this._apps;
    }

    ngOnInit(): void {
        this._appService.active = this._apps[0];
    }
}
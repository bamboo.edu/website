import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DesktopComponent } from './desktop.component';

const routes: Routes = [
    {
        path: "",
        component: DesktopComponent,
        children: [
            { path: "", redirectTo: "/terminal/welcome", pathMatch: "full" },
            { path: "none" },
            { path: "terminal", loadChildren: "../terminal/terminal.module#TerminalModule" },
            { path: "location", loadChildren: "../location/location.module#LocationModule" },
            { path: "contacts", loadChildren: "../contacts/contacts.module#ContactsModule" },
            { path: "signup", loadChildren: "../signup/signup.module#SignupModule" },
            { path: "teachers", loadChildren: "../teachers/teachers.module#TeachersModule" }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DesktopRoutingModule {}

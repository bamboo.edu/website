import { Component, Input } from '@angular/core';
import { Application } from '../model/application';
import { ApplicationService } from '../service/application';

@Component({
    selector: 'bamboo-dock',
    templateUrl: './dock.component.html',
    styleUrls: ['./dock.component.scss']
})
export class DockComponent {
    @Input('apps')
    apps: Application[] = [];

    constructor(private _appService: ApplicationService) {}

    run(app: Application): void {
        this._appService.active = app;
    }
}
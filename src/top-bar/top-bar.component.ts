import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import * as screenfull from 'screenfull';

@Component({
    selector: 'bamboo-top-bar',
    templateUrl: './top-bar.component.html',
    styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit, OnDestroy {
    private _time: string;
    private _tSubscription: Subscription;
    private _fullscreenEnabled: boolean = false;

    openURL(url: string) : void {
        window.open(url, "_blank");
    }

    get time() : string {
        return this._time;
    }

    get date() : string {
        let d: Date = new Date();
        return `${this._format(d.getDate())}/${this._format(d.getMonth())}/${d.getFullYear()}`;
    }

    get fullscreenEnabled() : boolean {
        return this._fullscreenEnabled;
    }

    get fullscreenSupported() : boolean {
        return screenfull.isEnabled;
    }

    toggleFullScreen() {
        if (screenfull.isEnabled) {
            screenfull.toggle();
        }
    }

    ngOnInit() : void {
        if (screenfull.isEnabled) {
            this._fullscreenEnabled = screenfull.isFullscreen;
            screenfull.on('change', () => {
                if (screenfull.isEnabled) {
                    this._fullscreenEnabled = screenfull.isFullscreen;
                }
            });
        }
        let timer = TimerObservable.create(0, 2000);
        this._tSubscription = timer.subscribe(() => {
            let d: Date = new Date();
            this._time = `${this._format(d.getHours())}:${this._format(d.getMinutes())}`;
        });
    }

    ngOnDestroy() {
        this._tSubscription.unsubscribe();
    }

    private _format(n: number) : string {
        let res: string = `${n}`;
        if (n < 10) {
            res = "0" + res;
        }
        return res;
    }
}
export interface Application {
    Icon: string;
    Path: string;
    Name: string;
    Title: string;
}
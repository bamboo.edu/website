export interface Email {
    subject?: string;
    body?: string;
    headers?: string[];
    to: string;
    from: string;
}
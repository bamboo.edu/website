import { Component } from '@angular/core';
import { Router, Event, NavigationStart } from '@angular/router';
import { ApplicationService } from '../service/application';

@Component({
    selector: 'bamboo-window',
    templateUrl: './window.component.html',
    styleUrls: ['./window.component.scss']
})
export class WindowComponent {
    private _opened: boolean = true;
    public scrollbarOptions = { axis: 'y', theme: 'minimal-dark' };

    constructor(
        private _appService: ApplicationService,
        private _router: Router) {
        this._validatePath(this._router.url);
        this._router.events.subscribe((event: Event) => {
            if(event instanceof NavigationStart) {
                this._validatePath(event.url);
            }
        });
    }

    closeWindow() : void {
        this._router.navigateByUrl('/none');
    }

    get isOpen() : boolean {
        return this._opened;
    }

    get title() : string {
        if (this._appService.active == null) {
            return "";
        }
        return this._appService.active.Name;
    }

    private _validatePath(url: string) : void {
        this._opened = url != "/none";
    }
}
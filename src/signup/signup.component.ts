import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { MailService } from '../service/mail';
import { Email } from '../model/email';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
    private _req: UserRequest = null;
    private _courses: Course[] = [
        {
            Name: "Разработка приложений на языке Python 3",
            Value: "python3"
        },
        {
            Name: "Разработка игр на Scratch 3",
            Value: "scratch3"
        },
        {
            Name: "Основы web-программирования",
            Value: "web"
        },
    ];
    private _sub: any;

    constructor(
        private _router: Router,
        private _mail: MailService,
        private _snackBar: MatSnackBar) {
        this._req = new UserRequest();
        this._sub = this._router.events.subscribe((event: Event) => {
            if(event instanceof NavigationEnd) {
                let value: string = event.url.split("/").pop();
                for (let course of this._courses) {
                    if (course.Value == value) {
                        this._req.Course = value;
                        break;
                    }
                }
            }
        });
    }
    
    ngOnInit() : void {}

    ngOnDestroy() : void {
        this._sub.unsubscribe();
    }

    get request() : UserRequest {
        return this._req;
    }

    signUp() : void {
        for (let course of this._courses) {
            if (course.Value == this._req.Course) {
                this._req.CourseName = course.Name;
                break;
            }
        }
        this._mail.send(this._emailFromTemplate(this._req)).subscribe(
            () => {
                this._showSnackBar("Ваша заявка принята! Мы свяжемся с вами в ближайшее время");
            },
            (err) => {
                this._showSnackBar(err.message);
            }
        );
    }

    get courses() : Course[] {
        return this._courses;
    }

    private _emailFromTemplate(req: UserRequest) : Email {
        let email: Email = {
            to: "gutorov.artem@yandex.ru",
            from: "gutorov.artem@yandex.ru",
            subject: "Новая заявка в IT-школу BAMBOO",
            headers: [
                "MIME-Version: 1.0",
                "Content-Type: text/html; charset=UTF-8"
            ],
            body: `${req.FirstName} ${req.LastName} \
            оставил заявку на изучение курса <b style="color: green;">${req.CourseName}</b> (код ${req.Course}).<br /><br /> \
            Телефон для связи: +375${req.Phone}`
        };
        return email;
    }

    private _showSnackBar(message: string) : void {
        this._snackBar.open(message, null, {
            duration: 5000,
            panelClass: "snack"
        });
    }
}

class UserRequest {
    FirstName?: string;
    LastName?: string;
    Phone: string;
    Course: string;
    CourseName?: string;
}

interface Course {
    Name: string;
    Value: string;
}
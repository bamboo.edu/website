import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationComponent } from './location.component';
import { LocationRoutingModule } from './location-routing.module';
import { MatCardModule } from '@angular/material/card';

@NgModule({
    declarations: [
        LocationComponent,
    ],
    imports: [
        LocationRoutingModule,
        CommonModule,
        MatCardModule
    ]
})
export class LocationModule {}